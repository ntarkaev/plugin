<?php

class Plugin_Name_i18n {

	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'events-trainee',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
