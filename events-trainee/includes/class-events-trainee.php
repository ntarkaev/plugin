<?php

class Events_Trainee {

    protected $loader;

    protected $plugin_name;

    protected $version;

    protected $widget;


    public function __construct() {
        if (defined('PLUGIN_NAME_VERSION')) {
            $this->version = PLUGIN_NAME_VERSION;
        } else {
            $this->version = '1.0.0';
        }
        $this->plugin_name = 'events-trainee';

        $this->load_dependencies();
        $this->set_locale();

        add_action( 'admin_print_styles', array( $this, 'hkdc_admin_styles' ) );
        add_action( 'wp_print_styles', array( $this, 'hkdc_admin_styles' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'hkdc_admin_scripts' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'hkdc_admin_scripts' ) );
        add_action( 'init', array( $this, 'create_taxonomy' ) );
        add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes_for_events' ), 1 );
        add_action( 'save_post', array( $this, 'extra_fields_update' ), 0 );
        add_action( 'init', array( $this, 'register_post_type_events' ) );
        add_action( 'widgets_init', array( $this, 'register_widget_events' ) );
        add_shortcode( 'eventstrainee', array( $this, 'create_shortcode_events' ) );

    }


    private function load_dependencies() {

        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-events-trainee-loader.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-events-trainee-i18n.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-timestamp.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-widget-events.php';

        $this->loader = new Events_Trainee_Loader();
        $this->widget = new Widget_Events();

    }
    private function set_locale()
    {

        $plugin_i18n = new Plugin_Name_i18n();

        $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');

    }

    public function run() {
        $this->loader->run();
    }

    public function get_plugin_name() {
        return $this->plugin_name;
    }

    public function get_loader() {
        return $this->loader;
    }

    public function get_version() {
        return $this->version;
    }
    public function hkdc_admin_styles() {
        wp_enqueue_style('jquery-ui-datepicker-style', '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css');
        wp_enqueue_style('events_widget_plugin', plugins_url('events-trainee') . '/css/style.css', false, false, 'all');
    }
    public function hkdc_admin_scripts() {
        wp_enqueue_script('jquery-ui-datepicker');
        wp_enqueue_script('te_js', plugins_url('events-trainee') . '/js/init_datepicker.js', array('jquery'), false, true);
        wp_enqueue_script('events_ajax', plugins_url('events-trainee') . '/js/ajax.js', false, false, true);
    }

    public function create_taxonomy(){
        register_taxonomy( 'events', array( 'events' ), array(
            'label'                 => '',
            'labels'                => array(
                'name'              => 'Events',
                'singular_name'     => 'Event',
                'search_items'      => 'Search events',
                'all_items'         => 'All events',
                'view_item '        => 'View event',
                'parent_item'       => 'Parent event',
                'parent_item_colon' => 'Parent event:',
                'edit_item'         => 'Edit event',
                'update_item'       => 'Update event',
                'add_new_item'      => 'Add New Event',
                'new_item_name'     => 'New Event Name',
                'menu_name'         => 'Event',
            ),
            'description'           => '',
            'public'                => true,
            'publicly_queryable'    => null,
            'show_in_nav_menus'     => false,
            'show_ui'               => false,
            'show_tagcloud'         => false,
            'show_in_rest'          => null,
            'rest_base'             => null,
            'hierarchical'          => false,
            'rewrite'               => true,
            'meta_box_cb'           => null,
            'show_admin_column'     => false,
            '_builtin'              => false,
            'show_in_quick_edit'    => null,
        ) );
    }

    public function add_meta_boxes_for_events() {
        add_meta_box( 'extra_fields', 'Дополнительная информация', array( $this, 'extra_fields_box' ), 'events', 'normal', 'high' );
    }

    public function extra_fields_box( $post ) {
        ?>
            <p>
                Статус:<br />
                <select name="extra[status_event]" style="width: 32%;">
                    <?php $sel_v = get_post_meta($post->ID, 'status_event', 1); ?>
                    <option value="none">- <?php _e( 'Не выбрано', 'events-trainee' ); ?> -</option>
                    <option value="1" <?php selected( $sel_v, '1' )?> ><?php _e( 'Открытое событие', 'events-trainee' ); ?></option>
                    <option value="2" <?php selected( $sel_v, '2' )?> ><?php _e( 'Только по приглашению', 'events-trainee' ); ?></option>
                </select>
            </p>
            <p>
                Дата события:<br />
                <?php
                $sel_v = get_post_meta($post->ID, 'date_event', 1);
                $sel_v = Timestamp::de_timestamp($sel_v);
                ?>
                <input style="width:400px;" id="jquery-datepicker" type="text" size="70" name="extra[date_event]" value="<?php echo $sel_v; ?>" />
            </p>
            <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />
        <?php
    }

    public function extra_fields_update( $post_id ) {
        if ( !isset( $_POST['extra_fields_nonce'] ) || !wp_verify_nonce( $_POST['extra_fields_nonce'], __FILE__ ) ) {
            return false;
        }
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return false;
        }

        if ( !current_user_can( 'edit_post', $post_id ) ) {
            return false;
        }
        if ( !isset( $_POST['extra'] ) ) {
            return false;
        }

        $_POST['extra'] = array_map( 'trim', $_POST['extra'] );
        foreach ( $_POST['extra'] as $key => $value ) {
            if ( empty( $value ) ) {
                delete_post_meta( $post_id, $key );
                continue;
            } elseif( $key == "date_event" ) {
                $value = Timestamp::to_timestamp( $value );
            }
            update_post_meta( $post_id, $key, $value );
        }
        return $post_id;
    }

	public function register_post_type_events() {
        register_post_type('events', array(
            'label'  => null,
            'labels' => array(
                'name'               => 'События',
                'singular_name'      => 'Событие',
                'add_new'            => 'Добавить событие',
                'add_new_item'       => 'Добавление события',
                'edit_item'          => 'Редактирование события',
                'new_item'           => 'Новое событие',
                'view_item'          => 'Смотреть событие',
                'search_items'       => 'Искать событие',
                'not_found'          => 'Не найдено',
                'not_found_in_trash' => 'Не найдено в корзине',
                'parent_item_colon'  => '',
                'menu_name'          => 'События',
            ),
            'description'         => '',
            'public'              => true,
            'publicly_queryable'  => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'menu_position'       => null,
            'hierarchical'        => false,
            'supports'            => array( 'title', 'extra_fields', ), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
            'taxonomies'          => array( 'events' ),
            'has_archive'         => true,
            'rewrite'             => true,
            'query_var'           => true,
        ) );
    }

    public function register_widget_events() {
        register_widget( 'Widget_Events' );
    }

    public function create_shortcode_events( $atts ) {

        $status_event = array( "none", "1", "2" );
        $number_of_events = 5;

        if( isset( $atts[ "status" ] ) ){
            if( $atts[ "status" ] == "open" ) {
                $status_event = 1;
            } elseif( $atts[ "status" ] == "close" ) {
                $status_event = 2;
            } elseif( $atts[ "status" ] == "all" ) {
                $status_event = array( "none", "1", "2" );
            }
        }
        if( isset( $atts[ "number" ] ) ){
            $number_of_events = (int) $atts[ "number" ];
            if( $number_of_events == 0 ) {
                $number_of_events = 5;
            }
        }
        $current_date = current_time( 'd.m.Y' );
        $current_timestamp = Timestamp::to_timestamp($current_date);

        $args = array(
            'numberposts' => $number_of_events,
            'meta_key'    => 'status_event',
            'meta_value'  => $status_event,
            'order_date'  => 'date_event',
            'post_type'   => 'events',
            'meta_query'  => array(
                array(
                    'key'     => 'date_event',
                    'value'   => $current_timestamp,
                    'compare' => '>=',
                ),
            ),
        );
        $posts = get_posts( $args );
        if($posts){
            foreach ( $posts as $post ) {
                if( !$post->post_title ){
                    continue;
                }
                ?>
                <div class="widget-events-trainee-event">
                    <div class="widget-events-trainee-event-title">
                        <?php echo $post->post_title; ?>
                    </div>
                    <div class="widget-events-trainee-event-status">
                        <?php
                        if ( $post->status_event == 1 ) {
                            _e( 'Открытое событие', 'events-trainee' );
                        } elseif ( $post->status_event == 2 ) {
                            _e( 'Только по приглашению', 'events-trainee' );
                        }

                        ?>
                    </div>
                    <div class="widget-events-trainee-event-date">
                        <?php
                        echo Timestamp::de_timestamp($post->date_event);
                        ?>
                    </div>
                </div>
                <?php
                unset( $month_event );
            }
        } else {
            ?>
            <div class="widget-events-trainee-content-notfound">
                <?php _e("Не найдено"); ?>
            </div>
            <?php
        }

    }

}
