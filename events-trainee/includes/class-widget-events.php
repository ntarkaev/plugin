<?php

class Widget_Events extends WP_Widget {

    public function __construct(){
        parent::__construct(
            'events_widget',
            __('События', 'events_widget_domain'),
            array( 'description' => 'Просмотр предстоящих событий.' )
        );

        wp_localize_script( 'jquery', 'ajaxurl',
            array(
                'url' => admin_url('admin-ajax.php')
            )
        );
        
        add_action( 'wp_ajax_widget_events_trainee', array( $this, 'ajax' ) );
        add_action( 'wp_ajax_nopriv_widget_events_trainee', array( $this, 'ajax' ) );
    }

    public function widget( $args, $instance ) {
        ?>
        <div class="widget-events-trainee">
            <div class="widget-events-trainee-title">
                - <?php _e( 'События', 'events-trainee' ); ?> -
            </div>
            <div class="widget-events-trainee-ajaxform">
                <label>
                    <input type="radio" name="widget-events-trainee-ajaxform-radio" value="none" <?php echo ( $instance['status_event'] == "none" ) ? "checked = checked" : ""; ?> />
                    - все
                </label><br />
                <label>
                    <input type="radio" name="widget-events-trainee-ajaxform-radio" value="1" <?php echo ( $instance['status_event'] == 1 ) ? "checked = checked" : ""; ?> />
                    - открытое событие
                </label><br />
                <label>
                    <input type="radio" name="widget-events-trainee-ajaxform-radio" value="2" <?php echo ( $instance['status_event'] == 2 ) ? "checked = checked" : ""; ?> />
                    - только по приглашению
                </label>
                <input type="number" name="widget-events-trainee-ajaxform-number" value="<?php echo $instance['number_of_events']; ?>" />
            </div>
            <div id="widget-events-trainee-content">
                &nbsp;
            </div>
        </div>
        <?php

    }

    public function form( $instance ) {
        $status_event = "";
        $number_of_events = "5";

        if ( !empty( $instance ) ) {
            $status_event = $instance[ "status_event" ];
            $number_of_events = $instance[ "number_of_events" ];
        }

        $status_id = $this->get_field_id( "status_event" );
        $status_name = $this->get_field_name( "status_event" );

        ?>
            <label for="<?php echo $status_id; ?>"><?php _e( 'Показывать события с статусом', 'events-trainee' ); ?>:</label><br />
            <select id="<?php echo $status_id; ?>" name="<?php echo $status_name; ?>">
                <option value="none">- <?php _e( 'Все', 'events-trainee' ); ?> -</option>
                <option value="1" <?php echo ( $status_event == 1 ) ? "selected = selected" : ""; ?> >
                    <?php _e( 'Открытое событие', 'events-trainee' ); ?>
                </option>
                <option value="2" <?php echo ( $status_event == 2 ) ? "selected = selected" : ""; ?> >
                    <?php _e( 'Только по пришлашению', 'events-trainee' ); ?>
                </option>
            </select><br /><br />
        <?php
        $number_of_events_id = $this->get_field_id( "number_of_events" );
        $number_of_events_name = $this->get_field_name( "number_of_events" );
        ?>
            <label for="<?php echo $number_of_events_id; ?>"><?php _e( 'Показывать количество последних событий', 'events-trainee' ); ?>:</label><br />
            <input id="<?php echo $number_of_events_id; ?>" type="number" name="<?php echo $number_of_events_name; ?>" value="<?php echo $number_of_events; ?>" /><br />
        <?php
    }

    public function update( $new_instance, $old_instance )  {
        $values = array();
        $values[ "status_event" ] = htmlentities( $new_instance[ "status_event" ] );
        $values[ "number_of_events" ] = htmlentities( $new_instance[ "number_of_events" ] );
        return $values;
    }

    public function ajax() {
        $status_event = $_POST[ 'status_event' ];
        $number_of_events = ( int ) $_POST[ 'number_of_events' ];

        if( $status_event == "none" ) {
            $status_event = array( "none", "1", "2" );
        }

        if( $number_of_events == 0 ) {
            $number_of_events = 5;
        }

        $current_date = current_time( 'd.m.Y' );
        $current_timestamp = Timestamp::to_timestamp($current_date);

        $args = array(
            'numberposts' => $number_of_events,
            'meta_key'    => 'status_event',
            'meta_value'  => $status_event,
            'order_date'  => 'date_event',
            'post_type'   => 'events',
            'meta_query'  => array(
                array(
                    'key'     => 'date_event',
                    'value'   => $current_timestamp,
                    'compare' => '>=',
                ),
            ),
        );
        $posts = get_posts( $args );
        if($posts){
            foreach ( $posts as $post ) {
                if( !$post->post_title ){
                    continue;
                }
                ?>
                <div class="widget-events-trainee-event">
                    <div class="widget-events-trainee-event-title">
                        <?php echo $post->post_title; ?>
                    </div>
                    <div class="widget-events-trainee-event-status">
                        <?php
                        if ( $post->status_event == 1 ) {
                            _e( 'Открытое событие', 'events-trainee' );
                        } elseif ( $post->status_event == 2 ) {
                            _e( 'Только по приглашению', 'events-trainee' );
                        }

                        ?>
                    </div>
                    <div class="widget-events-trainee-event-date">
                        <?php
                        echo Timestamp::de_timestamp($post->date_event);
                        ?>
                    </div>
                </div>
                <?php
                unset( $month_event );
            }
        } else {
            ?>
                <div class="widget-events-trainee-content-notfound">
                    <?php _e("Не найдено"); ?>
                </div>
            <?php
        }

        wp_die();
    }

}
