jQuery(document).ready(function($) {
    widgetEventsTraineeAjaxForm($)
});

jQuery("input[name='widget-events-trainee-ajaxform-radio']").on('change', function($){
    widgetEventsTraineeAjaxForm($);
});
jQuery("input[name='widget-events-trainee-ajaxform-number']").on('change', function($){
    widgetEventsTraineeAjaxForm($);
});
jQuery("input[name='widget-events-trainee-ajaxform-number']").on('keyup', function($){
    widgetEventsTraineeAjaxForm($);
});

function widgetEventsTraineeAjaxForm($) {
    var data = {
        action: 'widget_events_trainee',
        status_event: jQuery("input[name='widget-events-trainee-ajaxform-radio']:checked").val(),
        number_of_events: jQuery("input[name='widget-events-trainee-ajaxform-number']").val(),
    };

    jQuery.post(ajaxurl, data, function(response) {
        jQuery('#widget-events-trainee-content').html(response);
    });
}
