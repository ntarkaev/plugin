<?php
/*
Plugin Name: Events trainee
Description: Практическое задание №3
Version: 2.0.0
Author: Nikolay Tarkaev
*/

/*  Copyright 2018  Nikolay Tarkaev  (email: nikolaytarkaev@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

define( 'PLUGIN_NAME_VERSION', '2.0.0' );

require plugin_dir_path( __FILE__ ) . 'includes/class-events-trainee.php';

function run_events_trainee() {

    $plugin = new Events_Trainee();
    $plugin->run();

}
run_events_trainee();
