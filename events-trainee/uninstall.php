<?php

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

$allposts = get_posts( 'numberposts=-1&post_type=events&post_status=any' );

foreach( $allposts as $postinfo) {
    delete_post_meta( $postinfo->ID, 'extra_fields' );
}

